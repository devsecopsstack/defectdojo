# default workflow rules: Merge Request pipelines
spec:
  inputs:
    base-image:
      description: The Docker image used to run import
      default: registry.hub.docker.com/library/node:alpine3.11
    dir:
      description: DefectDojo working directory
      default: .
    server-url:
      description: URL of DefectDojo server
      default: ''
    noprod-enabled:
      description: Determines whether security reports produced in non-production branches are uploaded to DefectDojo
      type: boolean
      default: false
    timezone:
      description: Time zone used for naming imports in DefectDojo
      default: Europe/Paris
    smtp-server:
      description: name:port of SMTP server used for notifications - if this value is set, DefectDojo will send an e-mail notification in case of new vulnerabilities
      default: ''
    notification-severities:
      description: List of severities for which you want to be notified - DEFECTDOJO_SMTP_SERVER must be defined if you want to be notified
      default: Critical,High
    bandit-reports:
      description: path to Bandit JSON reports
      default: bandit*.json reports/py-bandit.bandit.json
    dc-reports:
      description: Path to Dependency Check reports
      default: dependency-check*.xml
    dc-gradle-reports:
      description: Path to Dependency Check reports from Gradle template
      default: dependency-check*.xml
    gitleaks-reports:
      description: Path to Gitleaks reports
      default: gitleaks/gitleaks-report.json reports/gitleaks.native.json
    hadolint-reports:
      description: Path to Hadolint reports
      default: hadolint-json-*.json reports/docker-hadolint-*.native.json
    mobsf-reports:
      description: Path to MobSF reports
      default: mobsf*.json
    nodejsscan-reports:
      description: Path to NodeJSScan reports
      default: nodejsscan-report-sarif.json
    npmaudit-reports:
      description: Path to NPMAudit reports
      default: npm-audit*.json
    testssl-reports:
      description: Path to TestSSL reports
      default: reports/testssl.native.csv
    trivy-reports:
      description: Path to Trivy reports
      default: trivy/*.json trivy-*.json reports/docker-trivy-*.native.json reports/py-trivy.trivy.json
    zap-reports:
      description: Path to Zap reports
      default: reports/zap.native.xml
    zap-tpl-project:
      description: Path to Zap template
      default: ''
    semgrep-reports:
      description: Path to Semgrep reports
      default: reports/semgrep.native.json
    semgrep-template:
      description: Path to Semgrep template
      default: ''
    sonarqube-sinceleakperiod:
      description: Determines if delta analysis is activated for SonarQube export
      type: boolean
      default: false
    sonarqube-nosecurityhotspot:
      description: Set this flag to true if SonarQube version does not support security hotspots (v < 7.3)
      type: boolean
      default: true
    sonarqube-allbugs:
      description: In SonarQube, determines if all bugs are exported (true) or only vulnerabilities (false)
      type: boolean
      default: false
---
workflow:
  rules:
    # prevent MR pipeline originating from production or integration branch(es)
    - if: '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ $PROD_REF || $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ $INTEG_REF'
      when: never
    # on non-prod, non-integration branches: prefer MR pipeline over branch pipeline
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_COMMIT_REF_NAME !~ $PROD_REF && $CI_COMMIT_REF_NAME !~ $INTEG_REF'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*tag(,[^],]*)*\]/" && $CI_COMMIT_TAG'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*branch(,[^],]*)*\]/" && $CI_COMMIT_BRANCH'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*mr(,[^],]*)*\]/" && $CI_MERGE_REQUEST_ID'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*default(,[^],]*)*\]/" && $CI_COMMIT_REF_NAME =~ $CI_DEFAULT_BRANCH'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*prod(,[^],]*)*\]/" && $CI_COMMIT_REF_NAME =~ $PROD_REF'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*integ(,[^],]*)*\]/" && $CI_COMMIT_REF_NAME =~ $INTEG_REF'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*dev(,[^],]*)*\]/" && $CI_COMMIT_REF_NAME !~ $PROD_REF && $CI_COMMIT_REF_NAME !~ $INTEG_REF'
      when: never
    - when: always

variables:
  # variabilized tracking image
  TBC_TRACKING_IMAGE: registry.gitlab.com/to-be-continuous/tools/tracking:master

  DEFECTDOJO_BASE_IMAGE: $[[ inputs.base-image ]]
  DEFECTDOJO_DIR: $[[ inputs.dir ]]
  DEFECTDOJO_SERVER_URL: $[[ inputs.server-url ]]
  DEFECTDOJO_SMTP_SERVER: $[[ inputs.smtp-server ]]
  DEFECTDOJO_NOTIFICATION_SEVERITIES: $[[ inputs.notification-severities ]]
  DEFECTDOJO_TIMEZONE: $[[ inputs.timezone ]]
  DEFECTDOJO_NOPROD_ENABLED: $[[ inputs.noprod-enabled ]]
  DEFECTDOJO_BANDIT_REPORTS: $[[ inputs.bandit-reports ]]
  DEFECTDOJO_GITLEAKS_REPORTS: $[[ inputs.gitleaks-reports ]]
  DEFECTDOJO_NODEJSSCAN_REPORTS: $[[ inputs.nodejsscan-reports ]]
  DEFECTDOJO_NPMAUDIT_REPORTS: $[[ inputs.npmaudit-reports ]]
  DEFECTDOJO_DC_REPORTS: $[[ inputs.dc-reports ]]
  DEFECTDOJO_DC_GRADLE_REPORTS: $[[ inputs.dc-gradle-reports ]]
  DEFECTDOJO_TRIVY_REPORTS: $[[ inputs.trivy-reports ]]
  DEFECTDOJO_HADOLINT_REPORTS: $[[ inputs.hadolint-reports ]]
  DEFECTDOJO_MOBSF_REPORTS: $[[ inputs.mobsf-reports ]]
  DEFECTDOJO_SEMGREP_REPORTS: $[[ inputs.semgrep-reports ]]
  SEMGREP_TEMPLATE: $[[ inputs.semgrep-template ]]  
  DEFECTDOJO_TESTSSL_REPORTS: $[[ inputs.testssl-reports ]]
  DEFECTDOJO_SONARQUBE_SINCELEAKPERIOD: $[[ inputs.sonarqube-sinceleakperiod ]]
  DEFECTDOJO_SONARQUBE_NOSECURITYHOTSPOT: $[[ inputs.sonarqube-nosecurityhotspot ]]
  DEFECTDOJO_SONARQUBE_ALLBUGS: $[[ inputs.sonarqube-allbugs ]]
  DEFECTDOJO_ZAP_REPORTS: $[[ inputs.zap-reports ]]
  ZAP_TPL_PROJECT: $[[ inputs.zap-tpl-project ]]

  # default production ref name (pattern)
  PROD_REF: /^(master|main)$/

###############################################################################################
#                                      scripts definition                                     #
###############################################################################################

.defectdojo-scripts: &defectdojo-scripts |
  # BEGSCRIPT
  set -e

  function log_info() {
      echo -e "[\\e[1;94mINFO\\e[0m] $*"
  }

  function log_warn() {
      echo -e "[\\e[1;93mWARN\\e[0m] $*"
  }

  function log_error() {
      echo -e "[\\e[1;91mERROR\\e[0m] $*"
  }

  function fail() {
    log_error "$*"
    exit 1
  }


  function install_custom_and_default_ca_certs() {
      if [[ -z "${DEFAULT_CA_CERTS}" ]]
      then
        echo "no default certs"
      else
        echo "${DEFAULT_CA_CERTS}" | tr -d '\r' >> /etc/ssl/certs/ca-certificates.crt
      fi
      if [[ -z "${CUSTOM_CA_CERTS}" ]]
      then
        echo "no custom certs"
      else
        echo "${CUSTOM_CA_CERTS}" | tr -d '\r' >> /etc/ssl/certs/ca-certificates.crt
      fi
      export NODE_EXTRA_CA_CERTS=/etc/ssl/certs/ca-certificates.crt
  }

  function write_ssmtp_conf() {
    apk add ssmtp
    log_info "ssmtp installed"
    cat > /etc/ssmtp/ssmtp.conf <<-EOF
  	root=postmaster
  	mailhub=$DEFECTDOJO_SMTP_SERVER
  	hostname=$(hostname)
  	FromLineOverride=YES
  	EOF
    log_info "ssmtp configured"
  }

  function prepare_mail_template() {
    log_info "prepare mail template"
    cat > /tmp/mail.txt <<-EOF
  	To: $GITLAB_USER_EMAIL
  	From: notif@defectdojo
  	Subject: New vulnerabilities in $CI_PROJECT_PATH
  	
  	Hello,
  	
  	DefectDojo found <<nb>> new $DEFECTDOJO_NOTIFICATION_SEVERITIES vulnerabilities in $CI_PROJECT_PATH.
  	EOF
  }

  function import_into_defectdojo() {
    # Check that mandatory DEFECTDOJO_API_KEY is set
    if [[ -z "$DEFECTDOJO_API_KEY" ]]; then
        log_error "missing DefectDojo AP2 v2 KEY"
        exit 1
    fi

    # Check that mandatory DEFECTDOJO_SERVER_URL is set
    if [[ -z "$DEFECTDOJO_SERVER_URL" ]]; then
        log_error "missing DefectDojo URL"
        exit 1
    fi

    # Download tzdata 
    apk add tzdata

    # Download curl
    apk add curl
    log_info "curl installed"

    # Download jq
    apk add jq
    log_info "jq installed"

    # Download and configure SSMTP if we are sending mails
    if [[ "$DEFECTDOJO_SMTP_SERVER" ]]; then
      write_ssmtp_conf
    fi

    # Custom CA certificates
    install_custom_and_default_ca_certs
    log_info "custom CA certs OK"

    # Install sonar-report (https://github.com/soprasteria/sonar-report)
    npm install -g sonar-report@2.2.1
    log_info "sonar-report installed"

    # Download yq
    YQ_VERSION=$(curl -sI "https://github.com/mikefarah/yq/releases/latest" | grep -i "^location:" | sed "s|.*/||" | tr -d '\r' | tr -d '\n')
    curl -sL -o ./yq "https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64"
    chmod +x ./yq
    log_info "YQ installed"
    
    _today=$(TZ="${DEFECTDOJO_TIMEZONE}" date +"%Y-%m-%d")
    _today_time=$(TZ="${DEFECTDOJO_TIMEZONE}" date +"%Y-%m-%d_%T")
    _current_time=$(TZ="${DEFECTDOJO_TIMEZONE}" date +"%H:%M")

    # search DefectDojo product (=project)
    curl -L "${DEFECTDOJO_SERVER_URL}/api/v2/products/?name=$CI_PROJECT_PATH" --header "Content-Type: application/json" --header "Authorization: Token $DEFECTDOJO_API_KEY" 1> api_search_product.json
    dd_product_pk=$(jq ".results[0].id" api_search_product.json)
    if [[ "${dd_product_pk}" == "null" ]]; then
      log_error "product not found (check API key)"
      exit 1
    fi
    log_info "dd_product_pk: ${dd_product_pk}"

    if [[ "$DEFECTDOJO_SMTP_SERVER" ]]; then
      prepare_mail_template
    fi

    log_info "call to API v2 ${DEFECTDOJO_SERVER_URL}/api/v2/findings/?test__engagement__product=${dd_product_pk}&severity=$DEFECTDOJO_NOTIFICATION_SEVERITIES&limit=100&false_p=false&duplicate=false&active=true" --header "Content-Type: application/json" --header "Authorization: Token $DEFECTDOJO_API_KEY" 
    curl -L "${DEFECTDOJO_SERVER_URL}/api/v2/findings/?test__engagement__product=${dd_product_pk}&severity=$DEFECTDOJO_NOTIFICATION_SEVERITIES&limit=100&false_p=false&duplicate=false&active=true" --header "Content-Type: application/json" --header "Authorization: Token $DEFECTDOJO_API_KEY" ${TRACE+--verbose} 1> api_init_findings.json
    init_vulns=$(jq ".results[] | .id" api_init_findings.json)
    nb_reports=0

    # ZAP 
    zap_nb_reports=0
    for file in ${DEFECTDOJO_ZAP_REPORTS}
    do
      if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
        log_info "zap report found: $file"
        zap_nb_reports=$((zap_nb_reports + 1))
        nb_reports=$((nb_reports + 1))
      fi
    done

    # Bandit 
    # template: python
    bandit_nb_reports=0
    for file in ${DEFECTDOJO_BANDIT_REPORTS}
    do
      if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
        log_info "bandit report found: $file"
        bandit_nb_reports=$((bandit_nb_reports + 1))
        nb_reports=$((nb_reports + 1))
      fi
    done

    # Sonar
    # template: sonar
    sonarqube_report=0
    if [[ "$SONAR_AUTH_TOKEN" ]] && [[ -z "$SONAR_TOKEN" ]]
    then 
      log_warn "\$SONAR_AUTH_TOKEN variable detected: use \$SONAR_TOKEN instead (see doc)"
      export SONAR_TOKEN="$SONAR_AUTH_TOKEN"
    fi
    if [ -n "${SONAR_URL}" ] && [ -n "${SONAR_TOKEN}" ] && [ -n "${DEFECTDOJO_SONARQUBE_PROJECT_KEY}" ] && [ -n "${DEFECTDOJO_SONARQUBE_APPLICATION}" ]; then
      sonar_report="sonar-report.html"
      log_info "sonar-report will call ${SONAR_URL}/api/issues/search?componentKeys=${DEFECTDOJO_SONARQUBE_PROJECT_KEY}&ps=500&p=1&statuses=OPEN,CONFIRMED,REOPENED&resolutions=&s=STATUS&asc=no&types=VULNERABILITY"
      sonar-report \
      --sonarurl="${SONAR_URL}" \
      --sonarcomponent="${DEFECTDOJO_SONARQUBE_PROJECT_KEY}" \
      --project="${DEFECTDOJO_SONARQUBE_PROJECT_KEY}" \
      --application="${DEFECTDOJO_SONARQUBE_APPLICATION}" \
      --branch="${CI_COMMIT_REF_NAME}" \
      --sonartoken="${SONAR_TOKEN}" \
      --sinceleakperiod="${DEFECTDOJO_SONARQUBE_SINCELEAKPERIOD}" \
      --noSecurityHotspot="${DEFECTDOJO_SONARQUBE_NOSECURITYHOTSPOT}" \
      --allbugs="${DEFECTDOJO_SONARQUBE_ALLBUGS}" > "$sonar_report"
      log_info "sonar-report launched"
      if [[ -f "$sonar_report" ]]; then
              log_info "$sonar_report exists."
              sonarqube_report=1
              nb_reports=$((nb_reports + 1))
      fi
    fi

    # Trivy
    # template: docker
    trivy_nb_reports=0
    for file in ${DEFECTDOJO_TRIVY_REPORTS}
    do
      if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
        log_info "trivy report found: $file"
        trivy_nb_reports=$((trivy_nb_reports + 1))
        nb_reports=$((nb_reports + 1))
      fi
    done

    # Hadolint
    # template: docker
    hadolint_nb_reports=0
    for file in ${DEFECTDOJO_HADOLINT_REPORTS}
    do
      if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
        log_info "hadolint report found: $file"
        hadolint_nb_reports=$((hadolint_nb_reports + 1))
        nb_reports=$((nb_reports + 1))
      fi
    done

    # Gitleaks
    # template: gitleaks
    gitleaks_nb_reports=0
    for file in ${DEFECTDOJO_GITLEAKS_REPORTS}
    do
      if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
        log_info "gitleaks report found: $file"
        #sed -i "s|^null$|{}|" $DEFECTDOJO_GITLEAKS_REPORTS
        gitleaks_nb_reports=$((gitleaks_nb_reports + 1))
        nb_reports=$((nb_reports + 1))
      fi
    done

    # MobSF
    # template: mobsf
    mobsf_nb_reports=0
    for file in ${DEFECTDOJO_MOBSF_REPORTS}
    do
      if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
        log_info "mobsf report found: $file"
        mobsf_nb_reports=$((mobsf_nb_reports + 1))
        nb_reports=$((nb_reports + 1))
      fi
    done

    # TestSSL
    # template: testssl
    testssl_nb_reports=0
    for file in ${DEFECTDOJO_TESTSSL_REPORTS}
    do
      if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
        log_info "testssl report found: $file"
        testssl_nb_reports=$((testssl_nb_reports + 1))
        nb_reports=$((nb_reports + 1))
      fi
    done

    # NodeJSScan
    # template: node
    nodejsscan_nb_reports=0
    for file in ${DEFECTDOJO_NODEJSSCAN_REPORTS}
    do
      if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
        log_info "nodejsscan report found: $file"
        nodejsscan_nb_reports=$((nodejsscan_nb_reports + 1))
        nb_reports=$((nb_reports + 1))
      fi
    done

    # NPM Audit
    # template: node
    npmaudit_nb_reports=0
    for file in ${DEFECTDOJO_NPMAUDIT_REPORTS}
    do
      if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
        log_info "npmaudit report found: $file"
        npmaudit_nb_reports=$((npmaudit_nb_reports + 1))
        nb_reports=$((nb_reports + 1))
      fi
    done

    # Dependency Check/Maven
    # template: maven
    dependency_check_nb_reports=0
    for file in ${DEFECTDOJO_DC_REPORTS}
    do
      if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
        log_info "Dependency Check/Maven report found: $file"
        dependency_check_nb_reports=$((dependency_check_nb_reports + 1))
        nb_reports=$((nb_reports + 1))
      fi
    done

    # Dependency Check/Gradle
    # template: gradle
    dependency_check_gradle_nb_reports=0
    for file in ${DEFECTDOJO_DC_GRADLE_REPORTS}
    do
      if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
        log_info "Dependency Check/Gradle report found: $file"
        dependency_check_gradle_nb_reports=$((dependency_check_gradle_nb_reports + 1))
        nb_reports=$((nb_reports + 1))
      fi
    done

    # Semgrep
    # template: semgrep
    semgrep_nb_reports=0
    for file in ${DEFECTDOJO_SEMGREP_REPORTS}
    do
      if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
        log_info "semgrep report found: $file"
        semgrep_nb_reports=$((semgrep_nb_reports + 1))
        nb_reports=$((nb_reports + 1))
      fi
    done    

    log_info "$nb_reports reports"

    if [ "$nb_reports" -gt 0 ]; then
      # prepare engagement creation
      _engname="Engagement ${_today_time} $CI_COMMIT_REF_NAME $CI_COMMIT_SHORT_SHA"
      _end=${_today}
      branch_tag=$CI_COMMIT_TAG
      branch_tag_info="[${CI_COMMIT_TAG}](${CI_PROJECT_URL}/-/tags/${CI_COMMIT_TAG})"
      # if there is no tag, then use branch
      if [[ -z "$CI_COMMIT_TAG" ]]; then
        branch_tag=$CI_COMMIT_REF_NAME
        branch_tag_info="[${CI_COMMIT_REF_NAME}](${CI_PROJECT_URL}/-/tree/${CI_COMMIT_REF_NAME})"
      fi
      dashboard_template_version=$(get_tpl_version_in_use "to-be-continuous/defectdojo")
      commit_info="[commit ${CI_COMMIT_SHORT_SHA}](${CI_PROJECT_URL}/-/commit/${CI_COMMIT_SHA})\n${branch_tag_info}\ncreated with dashboard-template ${dashboard_template_version}"
      echo "{\"engagement_type\": \"CI/CD\", \"product\": \"${dd_product_pk}\", \"name\": \"${_engname}\", \"source_code_management_uri\": \"${CI_PROJECT_URL}\", \"commit_hash\": \"${CI_COMMIT_SHA}\", \"branch_tag\": \"${branch_tag}\", \"status\": \"In Progress\", \"target_start\": \"${_today}\", \"target_end\": \"${_end}\", \"description\": \"${commit_info}\"}" > api_input.json

      # post request to create engagement
      curl -LX POST -d @api_input.json "${DEFECTDOJO_SERVER_URL}/api/v2/engagements/" --header "Content-Type: application/json" --header "Authorization: Token $DEFECTDOJO_API_KEY" ${TRACE+--verbose} 1> api_output.txt
      engagement_id=$(jq ".id" api_output.txt)
      echo "engagement_id: $engagement_id"

      if [ "$bandit_nb_reports" -gt 0 ]; then
        python_tpl_version=$(get_tpl_version_in_use "to-be-continuous/python")
        log_info "Python template version: $python_tpl_version"
        for file in ${DEFECTDOJO_BANDIT_REPORTS}
        do
          if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
            import_scan "$file" "Bandit Scan" "$engagement_id" "to-be-continuous/python ${python_tpl_version}"
          fi
        done
      fi

      if [ "$dependency_check_nb_reports" -gt 0 ]; then
        maven_tpl_version=$(get_tpl_version_in_use "to-be-continuous/maven")
        log_info "Maven template version: $maven_tpl_version"
        for file in ${DEFECTDOJO_DC_REPORTS}
        do
          if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
            import_scan "$file" "Dependency Check Scan" "$engagement_id" "to-be-continuous/maven ${maven_tpl_version}"
          fi
        done
      fi
      if [ "$dependency_check_gradle_nb_reports" -gt 0 ]; then
        gradle_tpl_version=$(get_tpl_version_in_use "to-be-continuous/gradle")
        log_info "Gradle template version: $gradle_tpl_version"
        for file in ${DEFECTDOJO_DC_GRADLE_REPORTS}
        do
          if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
            import_scan "$file" "Dependency Check Scan" "$engagement_id" "to-be-continuous/gradle ${gradle_tpl_version}"
          fi
        done
      fi
      if [ "$gitleaks_nb_reports" -gt 0 ]; then
        gitleaks_tpl_version=$(get_tpl_version_in_use "to-be-continuous/gitleaks")
        log_info "Gitleaks template version: $gitleaks_tpl_version"
        for file in ${DEFECTDOJO_GITLEAKS_REPORTS}
        do
          if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
            import_scan "$file" "Gitleaks Scan" "$engagement_id" "to-be-continuous/gitleaks ${gitleaks_tpl_version}"
          fi
        done
      fi
      if [ "$mobsf_nb_reports" -gt 0 ]; then
        mobsf_tpl_version=$(get_tpl_version_in_use "to-be-continuous/mobsf")
        log_info "MobSF template version: $mobsf_tpl_version"
        for file in ${DEFECTDOJO_MOBSF_REPORTS}
        do
          if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
            import_scan "$file" "MobSF Scan" "$engagement_id" "to-be-continuous/mobsf ${mobsf_tpl_version}"
          fi
        done
      fi
      if [ "$testssl_nb_reports" -gt 0 ]; then
        testssl_tpl_version=$(get_tpl_version_in_use "to-be-continuous/testssl")
        log_info "TestSSL template version: $testssl_tpl_version"
        for file in ${DEFECTDOJO_TESTSSL_REPORTS}
        do
          if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
            import_scan "$file" "Testssl Scan" "$engagement_id" "to-be-continuous/testssl ${testssl_tpl_version}"
          fi
        done
      fi
      if [ "$nodejsscan_nb_reports" -gt 0 ]; then
        node_tpl_version=$(get_tpl_version_in_use "to-be-continuous/node")
        log_info "Node template version: $node_tpl_version"
        for file in ${DEFECTDOJO_NODEJSSCAN_REPORTS}
        do
          if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
            import_scan "$file" "SARIF" "$engagement_id" "to-be-continuous/node ${node_tpl_version}"
          fi
        done
      fi
      if [ "$npmaudit_nb_reports" -gt 0 ]; then
        node_tpl_version=$(get_tpl_version_in_use "to-be-continuous/node")
        log_info "Node template version: $node_tpl_version"
        for file in ${DEFECTDOJO_NPMAUDIT_REPORTS}
        do
          if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
            import_scan "$file" "NPM Audit Scan" "$engagement_id" "to-be-continuous/node ${node_tpl_version}"
          fi
        done
      fi
      if [ "$sonarqube_report" -eq 1 ]; then
        file="sonar-report.html"
        sonar_tpl_version=$(get_tpl_version_in_use "to-be-continuous/sonar")
        log_info "Sonar template version: $sonar_tpl_version"
        import_scan "$file" "SonarQube Scan" "$engagement_id" "to-be-continuous/sonar ${sonar_tpl_version}"
      fi
      if [ "$trivy_nb_reports" -gt 0 ]; then
        docker_tpl_version=$(get_tpl_version_in_use "to-be-continuous/docker")
        log_info "Docker template version: $docker_tpl_version"
        for file in ${DEFECTDOJO_TRIVY_REPORTS}
        do
          if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
            artifact_type=$(jq -r ".ArtifactType" "${file}")
            artifact_name=$(jq -r ".ArtifactName" "${file}")
            if [ "${artifact_type}" == "container_image" ]; then
              service="${artifact_name}"
            else
              service=""
            fi
            import_scan "$file" "Trivy Scan" "$engagement_id" "to-be-continuous/docker ${docker_tpl_version}" "${service}"
          fi
        done
      fi
      if [ "$hadolint_nb_reports" -gt 0 ]; then
        docker_tpl_version=$(get_tpl_version_in_use "to-be-continuous/docker")
        log_info "Docker template version: $docker_tpl_version"
        for file in ${DEFECTDOJO_HADOLINT_REPORTS}
        do
          if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
            import_scan "$file" "Hadolint Dockerfile check" "$engagement_id" "to-be-continuous/docker ${docker_tpl_version}"
          fi
        done
      fi
      if [ "$zap_nb_reports" -gt 0 ]; then
        zap_tpl_version=$(get_tpl_version_in_use "$ZAP_TPL_PROJECT")
        log_info "zap template version: $zap_tpl_version"
        for file in ${DEFECTDOJO_ZAP_REPORTS}
        do
          if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
            import_scan "$file" "ZAP Scan" "$engagement_id" "$ZAP_TPL_PROJECT ${zap_tpl_version}"
          fi
        done
      fi

      if [ "$semgrep_nb_reports" -gt 0 ]; then
        semgrep_tpl_version=$(get_tpl_version_in_use "$SEMGREP_TEMPLATE")
        log_info "Semgrep template version: $semgrep_tpl_version"
        for file in ${DEFECTDOJO_SEMGREP_REPORTS}
        do
          if [[ $(expr "$file" : '.*\*.*') == 0 ]] && [[ -f "$file" ]]; then
            import_scan "$file" "Semgrep JSON Report" "$engagement_id" "$SEMGREP_TEMPLATE ${semgrep_tpl_version}"
          fi
        done
      fi
    
      # Close the engagement
      curl -L -X POST "${DEFECTDOJO_SERVER_URL}/api/v2/engagements/$engagement_id/close/" --header "Authorization: Token $DEFECTDOJO_API_KEY" -d ''

      curl -L "${DEFECTDOJO_SERVER_URL}/api/v2/findings/?test__engagement__product=${dd_product_pk}&severity=$DEFECTDOJO_NOTIFICATION_SEVERITIES&limit=100&false_p=false&duplicate=false&active=true" --header "Content-Type: application/json" --header "Authorization: Token $DEFECTDOJO_API_KEY" ${TRACE+--verbose} 1> api_final_findings.json
      final_vulns=$(jq ".results[] | .id" api_final_findings.json)
      nb_new_vulns=0
      for final_id in ${final_vulns}; do
        found_in_init=false
        for init_id in ${init_vulns}; do
          if [[ "${init_id}" == "${final_id}" ]]; then
            found_in_init=true
          fi
        done
        if [[ ${found_in_init} == false ]]; then
          log_warn "new finding #${final_id}"
          nb_new_vulns=$((nb_new_vulns + 1))
        fi
      done
      log_info "$nb_new_vulns new vulnerabilities"

      if [[ -n "$DEFECTDOJO_SMTP_SERVER" && ${nb_new_vulns} -gt 0 ]]; then
        log_info "set nb in mail template"
        sed -i 's/<<nb>>/'$nb_new_vulns'/' /tmp/mail.txt
        ssmtp "$GITLAB_USER_EMAIL" < /tmp/mail.txt
      fi
    else
      log_warn "no report"
      exit 0
    fi
  }

  get_tpl_version_in_use() {
    local template_repo=$1
    template_version=$(./yq eval --exit-status '.include[] | select(.project == "'"${template_repo}"'") | .ref' .gitlab-ci.yml 2>/dev/null || echo "N/A")
    echo "$template_version" | cut -d " " -f1
  }

  import_scan() {
    _file=$1
    _type=$2
    _engagement_id=$3
    _template_version=$4
    _scan_date=${_today}
    _scan_time=${_current_time}

    if [[ -z "$5" ]]; then
      _service=""
    else
      _service="$5"
    fi


    # get test_type id
    curl -L "${DEFECTDOJO_SERVER_URL}/api/v2/test_types/?limit=200" --header "Content-Type: application/json" --header "Authorization: Token $DEFECTDOJO_API_KEY" ${TRACE+--verbose} 1> api_test_type.json
    dd_test_type_pk=$(jq '.results[] | select(.name == "'"${_type}"'") | .id' api_test_type.json)
    log_info "dd_test_type_pk: ${dd_test_type_pk}"

    # post request to import scan
    log_info "try to import scan ${_file} ${_scan_date} ${_type}"
    curl -LX POST "${DEFECTDOJO_SERVER_URL}/api/v2/import-scan/" -H "Content-Type: multipart/form-data" -H "Authorization: Token $DEFECTDOJO_API_KEY" -F file=@"${_file}" -F scan_date="${_scan_date}" -F scan_type="${_type}" -F engagement="${_engagement_id}" -F close_old_findings="true" -F close_old_findings_product_scope="true" -F service="${_service}" ${TRACE+--verbose} 1> test.txt
    test_pk=$(jq ".test" test.txt)
    log_info "test_pk: ${test_pk}"
    if [[ -z "${test_pk}" ]]; then
      log_error "${_type} not imported"
      exit 1
    else
      log_info "${_type} report correctly imported"
    fi
  }

  function unscope_variables() {
    _scoped_vars=$(env | awk -F '=' "/^scoped__[a-zA-Z0-9_]+=/ {print \$1}" | sort)
    if [[ -z "$_scoped_vars" ]]; then return; fi
    log_info "Processing scoped variables..."
    for _scoped_var in $_scoped_vars
    do
      _fields=${_scoped_var//__/:}
      _condition=$(echo "$_fields" | cut -d: -f3)
      case "$_condition" in
      if) _not="";;
      ifnot) _not=1;;
      *)
        log_warn "... unrecognized condition \\e[1;91m$_condition\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
      ;;
      esac
      _target_var=$(echo "$_fields" | cut -d: -f2)
      _cond_var=$(echo "$_fields" | cut -d: -f4)
      _cond_val=$(eval echo "\$${_cond_var}")
      _test_op=$(echo "$_fields" | cut -d: -f5)
      case "$_test_op" in
      defined)
        if [[ -z "$_not" ]] && [[ -z "$_cond_val" ]]; then continue; 
        elif [[ "$_not" ]] && [[ "$_cond_val" ]]; then continue; 
        fi
        ;;
      equals|startswith|endswith|contains|in|equals_ic|startswith_ic|endswith_ic|contains_ic|in_ic)
        # comparison operator
        # sluggify actual value
        _cond_val=$(echo "$_cond_val" | tr '[:punct:]' '_')
        # retrieve comparison value
        _cmp_val_prefix="scoped__${_target_var}__${_condition}__${_cond_var}__${_test_op}__"
        _cmp_val=${_scoped_var#"$_cmp_val_prefix"}
        # manage 'ignore case'
        if [[ "$_test_op" == *_ic ]]
        then
          # lowercase everything
          _cond_val=$(echo "$_cond_val" | tr '[:upper:]' '[:lower:]')
          _cmp_val=$(echo "$_cmp_val" | tr '[:upper:]' '[:lower:]')
        fi
        case "$_test_op" in
        equals*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val" ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val" ]]; then continue; 
          fi
          ;;
        startswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val"* ]]; then continue; 
          fi
          ;;
        endswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val" ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val" ]]; then continue; 
          fi
          ;;
        contains*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val"* ]]; then continue; 
          fi
          ;;
        in*)
          if [[ -z "$_not" ]] && [[ "__${_cmp_val}__" != *"__${_cond_val}__"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "__${_cmp_val}__" == *"__${_cond_val}__"* ]]; then continue; 
          fi
          ;;
        esac
        ;;
      *)
        log_warn "... unrecognized test operator \\e[1;91m${_test_op}\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
        ;;
      esac
      # matches
      _val=$(eval echo "\$${_target_var}")
      log_info "... apply \\e[32m${_target_var}\\e[0m from \\e[32m\$${_scoped_var}\\e[0m${_val:+ (\\e[33;1moverwrite\\e[0m)}"
      _val=$(eval echo "\$${_scoped_var}")
      export "${_target_var}"="${_val}"
    done
    log_info "... done"
  }


  # evaluate and export a secret
  # - $1: secret variable name
  function eval_secret() {
    name=$1
    value=$(eval echo "\$${name}")
    case "$value" in
    @b64@*)
      decoded=$(mktemp)
      errors=$(mktemp)
      if echo "$value" | cut -c6- | base64 -d > "${decoded}" 2> "${errors}"
      then
        # shellcheck disable=SC2086
        export ${name}="$(cat ${decoded})"
        log_info "Successfully decoded base64 secret \\e[33;1m${name}\\e[0m"
      else
        fail "Failed decoding base64 secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
      fi
      ;;
    @hex@*)
      decoded=$(mktemp)
      errors=$(mktemp)
      if echo "$value" | cut -c6- | sed 's/\([0-9A-F]\{2\}\)/\\\\x\1/gI' | xargs printf > "${decoded}" 2> "${errors}"
      then
        # shellcheck disable=SC2086
        export ${name}="$(cat ${decoded})"
        log_info "Successfully decoded hexadecimal secret \\e[33;1m${name}\\e[0m"
      else
        fail "Failed decoding hexadecimal secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
      fi
      ;;
    @url@*)
      url=$(echo "$value" | cut -c6-)
      if command -v curl > /dev/null
      then
        decoded=$(mktemp)
        errors=$(mktemp)
        if curl -s -S -f --connect-timeout 5 -o "${decoded}" "$url" 2> "${errors}"
        then
          # shellcheck disable=SC2086
          export ${name}="$(cat ${decoded})"
          log_info "Successfully curl'd secret \\e[33;1m${name}\\e[0m"
        else
          log_warn "Failed getting secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
        fi
      elif command -v wget > /dev/null
      then
        decoded=$(mktemp)
        errors=$(mktemp)
        if wget -T 5 -O "${decoded}" "$url" 2> "${errors}"
        then
          # shellcheck disable=SC2086
          export ${name}="$(cat ${decoded})"
          log_info "Successfully wget'd secret \\e[33;1m${name}\\e[0m"
        else
          log_warn "Failed getting secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
        fi
      else
        fail "Couldn't get secret \\e[33;1m${name}\\e[0m: no http client found"
      fi
      ;;
    esac
  }

  function eval_all_secrets() {
    encoded_vars=$(env | grep -v '^scoped__' | awk -F '=' '/^[a-zA-Z0-9_]*=@(b64|hex|url)@/ {print $1}')
    for var in $encoded_vars
    do
      eval_secret "$var"
    done
  }

  unscope_variables
  eval_all_secrets

  # ENDSCRIPT


###############################################################################################
#                                        defectdojo job                                       # 
###############################################################################################
defectdojo:
  image:
    name: $DEFECTDOJO_BASE_IMAGE
    entrypoint: [""]
  services:
    - name: "$TBC_TRACKING_IMAGE"
      command: ["--service", "defectdojo", "2.6.4"]
  stage: .post
  before_script:
    - cd ${DEFECTDOJO_DIR}
  script:
    - !reference [.defectdojo-scripts]
    - import_into_defectdojo
  rules:
    # on production branch
    - if: '$CI_COMMIT_REF_NAME =~ $PROD_REF || $DEFECTDOJO_NOPROD_ENABLED == "true"'
      when: always
